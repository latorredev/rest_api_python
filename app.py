#!/usr/bin/env python3

from flask import Flask, jsonify # Importar framework y modulo
from  products import products # Importar archivo externo

app = Flask(__name__) 

@app.route('/ping', methods=['GET']) # Ruta de testeo
def ping():
    return jsonify({"Message" : "pong!"})

@app.route('/products', methods = ['GET']) # Ruta para visualizar productos
def get_products():
    return jsonify({"products" : products}, {"message" : "product's list"})

@app.route('/product/<string:product_name>') # Ruta para visualizar producto especificado por la aplicacion cliente
def get_product(product_name):
    print(product_name)
    return "received"


if __name__ == "__main__": # Setear el archivo actual como principal de la aplicacion
    app.run(debug=True, port=5000) # Iniciar servidor Flask
